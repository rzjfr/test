module ApplicationHelper
  def make_title(control, action)
    if control == "Static_pages"
      action.to_str + " | Ruby on Rails Tutorial Sample App"
    else
      control.to_str + " | Ruby on Rails Tutorial Sample App"
    end
  end
end
